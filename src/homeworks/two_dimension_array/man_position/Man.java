package homeworks.two_dimension_array.man_position;

public class Man {
    private Occupancy occupancy;

    private String firstName;

    private String lastName;

    private Gender gender;

    private int age = -999;

    public Man(Occupancy occupancy, String firstName, String lastName, Gender gender, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
        this.occupancy = occupancy;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Occupancy getOccupancy() {
        return occupancy;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Gender getGender() {
        return gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setOccupancy(Occupancy occupancy) {
        this.occupancy = occupancy;
    }
}

class TestAssociation {
    public static void main(String[] args) {

        ManService service = new ManService();

        Occupancy occupancy = new Occupancy("Account Manager");

        Occupancy occupancy1 = new Occupancy("Sales Manager");

        Man man1 = new Man(occupancy, "John", "Smith", Gender.MAN, 30);

        Man man2 = new Man(occupancy, "Michael", "Smith1", Gender.MAN, 20);

        service.compareLastNames(man1, man2);

        Occupancy occupancy2 = new Occupancy("Purchase Manager");

    }
}