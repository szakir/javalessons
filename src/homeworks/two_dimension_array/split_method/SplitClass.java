package homeworks.two_dimension_array.split_method;

public class SplitClass {

    public static void main(String[] args) {

        String text = "Assessment&Projects&MindTap";

        String[] words = text.split("&");

        String longest = getLongest(words);

        System.out.println("longest string is:" + longest);

        String shortest = getShortest(words);

        System.out.println("shortest string is: " + shortest);
    }

    public static String getLongest(String[] array) {
        int index = 0;

        int element = array[0].length();

        for (int i = 1; i < array.length; i++) {
            if (array[i].length() > element) {
                index = i;
                element = array[i].length();
            }
        }
        return array[index];
    }

    public static String getShortest(String[] array) {
        int index = 0;

        int element = array[0].length();

        for (int i = 0; i < array.length; i++) {
            if (array[i].length() < element) {
                element = array[i].length();
                index = i;
            }
        }

        return array[index];
    }




}
