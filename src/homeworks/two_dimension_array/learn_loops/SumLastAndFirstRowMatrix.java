package homeworks.two_dimension_array.learn_loops;

public class SumLastAndFirstRowMatrix {
    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {5, 6, 7, 8},
                {5, 6, 7, 8},
                {5, 6, 7, 8}
        };

        int sumFirstRow = 0;

        int sumLastRow = 0;

        for (int numberRow = 0; numberRow < matrix[0].length; numberRow++) {


            sumFirstRow += matrix[0][numberRow];

            sumLastRow += matrix[matrix.length - 1][numberRow];

        }

        /*for (int i = 0; i < matrix.length; i++) {
            for (int numberRow = 0; numberRow < matrix[i].length; numberRow++) {

                if (i == 0) {
                    sumFirstRow += matrix[i][numberRow];
                }

                if (i == matrix.length - 1) {
                    sumLastRow += matrix[i][numberRow];
                }

            }
        }*/

        System.out.println("Sum first line =  " + sumFirstRow + "\nSum last line = " + sumLastRow);

        int a =2;
        int b =3;

        System.out.println(++b + b++);
    }

}
