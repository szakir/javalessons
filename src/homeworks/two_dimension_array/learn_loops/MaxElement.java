package homeworks.two_dimension_array.learn_loops;

import java.util.Arrays;

public class MaxElement {
    public static void main(String[] args) {
        int[][] array = {
                {10, 20, 30, 40},
                {50, 50, 60, 70}
        };

        for (int i = 0; i < array.length; i++) {
            int max = array[i][0];

            int min = array[i][0];

            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                }

                if (array[i][j] < min) {
                    min = array[i][j];
                }

            }

            System.out.println("Max element of " + i + " line = " + max +
                                "\tMin element of " + i + " line = " + min);
        }

    }
}

/*
* iteri 0 -> i == 0
* {
    * iterj = 0 -> [0;0]
    * iterj = 1 -> [0;1]
    * iterj = 2 -> [0;2]
    * iterj = 3 -> [0;3]
* }
 * iteri 1 -> i == 1
* {
     * iterj = 0 -> [1;0]
     * iterj = 1 -> [1;1]
     * iterj = 2 -> [1;2]
     * iterj = 3 -> [1;3]
* }
* */



