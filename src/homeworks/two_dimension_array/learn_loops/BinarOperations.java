package homeworks.two_dimension_array.learn_loops;

public class BinarOperations {
    public static void main(String[] args) {

        System.out.println(Integer.toBinaryString(12)); //00001100
        System.out.println(Integer.toBinaryString(15)); //00001111
        System.out.println("12 & 15 = " + (12 & 15));    //00001100
        System.out.println("12 | 15 = " + (12 | 15));     //00001111


        System.out.println(Integer.toBinaryString(123)); //01111011
        System.out.println(Integer.toBinaryString(89));  //01011001
        System.out.println("123 & 89 = " + (123 & 89));     //01011001
        System.out.println("123 | 89 = " + (123 | 89));     //01111011


        System.out.println("123 << 5 = " + (123 << 5)); //123*2(5)
        System.out.println("123 >> 5 = " + (123 >> 5)); // 123/2(5)
        System.out.println("89 << 5 = " + (89 << 5));   // 89 * 2(5)
        System.out.println("89 >> 5 = " + (89 >> 5));   // 89 / 2(5)

    }
}
