package homeworks.two_dimension_array.learn_loops;

public class IncrementDecrement {
    public static void main(String[] args) {
        int count = 5;
        int r = count++ - ++count + count-- + ++count - --count;
//              (5 - 7 + 7 +6 -5)

        System.out.println(r);
    }
}
