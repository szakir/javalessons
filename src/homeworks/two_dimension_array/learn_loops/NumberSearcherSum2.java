package homeworks.two_dimension_array.learn_loops;

import java.util.Random;

public class NumberSearcherSum2 {
    public static void main(String[] args) {
        int[][] matrix2 = new int[5][5];

        Random random = new Random();


        for (int row = 0; row < matrix2.length; row++) {
            for (int column = 0; column < matrix2[row].length; column++) {

                matrix2[row][column] = random.nextInt(89) + 10;

                System.out.print(matrix2[row][column] + "\t");
            }

            System.out.println();
        }

        System.out.println("#################################");

            for (int i = 0; i < matrix2.length; i++) {
                for (int j = 0; j < matrix2[i].length; j++) {
                    int element = matrix2[i][j];

                    int countDozens = element / 10;

                    int countNumbers = element % 10;

                    if ((countDozens + countNumbers) % 2 == 0) {
                        System.out.print(element + "\t");
                    }
                }
            }

        }

}

