package homeworks.two_dimension_array.learn_loops;

public class MaxElmentII {
    public static void main(String[] args) {
        int[][] array = {
                {10, 20, 30, 40},
                {50, 50, 60, 70}
        };

        for (int i = 0; i < array.length; i++) {
            int max = array[i][0];

            int min = array[i][0];

            for (int j = 0; j < array[i].length; j++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                }

                if (array[i][j] < min) {
                    min = array[i][j];
                }

            }

            System.out.println("Max element of " + i + " line = " + max +
                    "\tMin element of " + i + " line = " + min);
        }
    }
}
