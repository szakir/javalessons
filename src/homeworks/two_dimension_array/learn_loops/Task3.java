package homeworks.two_dimension_array.learn_loops;


import java.util.Random;

public class Task3 {
    public static void main(String[] args) {
        int matrix[][] = new int[5][6];//0

        Random random = new Random();

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                matrix[i][j] = random.nextInt(50);
                System.out.print(matrix[i][j] + "\t");
            }

            System.out.println();
        }

        for(int i = 0; i < matrix[0].length; i++) {//[i] 0 - 4

            int min = matrix[0][i];

            int max = matrix[0][i];

            int sum1 = 0;

            for (int j = 0; j < matrix.length; j++) {

                if (matrix[j][i] > max) {
                    max = matrix[j][i];
                }

                if (matrix[j][i] < min) {
                    min = matrix[j][i];
                }

                sum1 += matrix[j][i];

            }

            System.out.println("Sum of " + i + " column = " + sum1);

            System.out.println("Max of " + i + " column = " + max + "\nMin of " + i + " column = " + min);

        }
    }
}

/*
* outer for, 0 iteration: row = 0, max, min - matrix[0][0]
*       inner for, 0 iteration: row = 0; column = 0, matrix[0][column] - int min, max = [0][0] - 32
*       inner for, 1 iteration: row = 0; column = 1, matrix[0][1] - int min, max = [0][0] - 36
*       inner for, 2 iteration: row = 0; column = 2
*       inner for, 3 iteration: row = 0; column = 3
*       inner for, 4 iteration: row = 0; column = 4, max = 42
*
*       outer for, 1 iteration: row = 1
    *       inner for, 0 iteration: row = 1; column = 0
    *       inner for, 1 iteration: row = 1; column = 1
    *       inner for, 2 iteration: row = 1; column = 2
    *       inner for, 3 iteration: row = 1; column = 3
    *       inner for, 4 iteration: row = 1; column = 4
* */


