package homeworks.two_dimension_array.learn_loops;

public class NumberSearcher {
    public static void main(String[] args) {
        int[][] matrix = {
                {10, 20, 30, 50, 50},
                {50, 55, 60, 70, 75},
                {80, 85, 90, 50, 95},
        };

        int[] array = {5, 0, 4, 8};

        for (int i = 0; i < array.length; i++) {

            int element = array[i];

            array[i] += 2;

            System.out.print(element + "\t");
        }

    }
}

        /* for (int element : array) {
            element += 2;
            System.out.print(element + "\t");
        }

        System.out.println();

        for (int element : array) {
            System.out.print(element + "\t");
        }

        int count = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {

                if (matrix[i][j] == 50) {
                    count++;
                }
            }

        }

//        System.out.println(count); */



