package homeworks.two_dimension_array.learn_loops;

public class NumberSearcher4 {
    public static void main(String[] args) {
        int[][] array = {
                {200, 300, 129, 336},
                {120, 180, 210, 240},
                {210, 264, 285, 256},
                {350, 360, 380, 390}
        };

        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                int element = array[i][j];

                int countHundreds = element / 100;

                int countDozens = element / 10;

                int countNumbers = element % 10;

                if ((countHundreds + countDozens + countNumbers) % 2 == 0 ) {
                    System.out.print(element + "\t");
                }
            }
        }


    }
    }

