package homeworks.two_dimension_array.learn_class;

public class CalculatorA {
    private double operandOne;//rename 0.0
    private double operandTwo;//rename 0.0

    public CalculatorA(double operandOne, double operandTwo) {//double operandOne = 2.0
        this.operandOne = operandOne;
        this.operandTwo = operandTwo;
    }

    public void setOperandOne(double operandOne) {
        this.operandOne = operandOne;
    }

    public void setOperandTwo(double operandTwo) {
        this.operandTwo = operandTwo;
    }

    /*  access modificator return type(or void) nameOfMethod(incoming parameters or absent them) {

 }*/

    public double sum() {
        double sum = operandOne + operandTwo;
        return sum;
    }

    public double minus() {
        double minus = operandOne - operandTwo;
        return minus;
    }

    public double multiplicate() {
        double multiplicate = operandOne * operandTwo;
        return multiplicate;
    }

    public double devide(){
        double devide = operandOne / operandTwo;
        return devide;
    }





    public static void main(String[] args) {

        CalculatorA calculator = new CalculatorA(2.0, 4.0);

        double sum = calculator.sum();

        System.out.println(sum);

        calculator.setOperandOne(6.0);

        calculator.setOperandTwo(5.0);


        double minus = calculator.minus();

        System.out.println(minus);

        calculator.setOperandOne(10.00);

        calculator.setOperandOne(5.00);


        double multiplicate = calculator.multiplicate();

        System.out.println(multiplicate);

        calculator.setOperandOne(105.00);

        calculator.setOperandOne(120.00);


        double devide = calculator.devide();

        System.out.println(devide);

        calculator.setOperandOne(244.00);

        calculator.setOperandOne(12.00);



    }

}

