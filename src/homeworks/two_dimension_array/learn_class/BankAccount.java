package homeworks.two_dimension_array.learn_class;

public class BankAccount {

    private double balance;

    public BankAccount() {
        balance = 200.00;
    }

    public void withDrawal(double booked) {//add if
        if (booked > balance) {
            System.out.println("Пополните счет!");//in English

            return;
        }

        balance = balance - booked;
    }

    public void putMoney(double checkIn) {
        balance = balance + checkIn;
    }

    public double getBalance() {
        return balance;
    }

    public static void main(String[] args) {
        BankAccount account = new BankAccount();

        account.withDrawal(80.00);

        account.putMoney( 50);

        System.out.println(account.getBalance());
    }
}


