package homeworks.two_dimension_array.learn_class;

public class CalculatorB {
    public double sum(double operandOne, double operandTwo) {
        double sum = operandOne + operandTwo;

        return sum;
    }


    public double minus(double operandOne, double operandTwo) {
        return operandOne - operandTwo;
    }

    public double multiply (double operandOne, double operandTwo) { return operandOne * operandTwo;}

    public double devide (double operandOne, double operandTwo) { return operandOne / operandTwo;}

    public static void main(String[] args) {
        CalculatorB calculator = new CalculatorB();

        double sum = calculator.sum(2.0, 4.0);

        System.out.println(sum);

        double minus = calculator.minus(10.00, 5.00);

        System.out.println(minus);

        double multiply = calculator.multiply(120.00, 130.00);

        System.out.println(multiply);

        double devide = calculator.devide(125.00, 5.00);

    }
}
