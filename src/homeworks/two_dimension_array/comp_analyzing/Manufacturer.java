package homeworks.two_dimension_array.comp_analyzing;

public enum Manufacturer {
    HP("HP"),
    APPLE("Apple"),
    LENOVO("Lenovo"),
    SAMSUNG("Samsung"),
    TOSHIBA("Toshiba");

    private String nameManufacturer;

    Manufacturer(String nameManufacturer) {
        this.nameManufacturer = nameManufacturer;
    }

    public String getNameManufacturer() {
        return nameManufacturer;
    }

}
