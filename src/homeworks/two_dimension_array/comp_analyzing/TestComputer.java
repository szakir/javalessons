package homeworks.two_dimension_array.comp_analyzing;

import java.util.ArrayList;

public class TestComputer {
    public static void main(String[] args) {
       Computer[] computers = new Computer[5];

        Manufacturer manuf = Manufacturer.LENOVO;

        Computer computer = new Computer(Manufacturer.LENOVO, 12256, 1200, 2, 2500);

        Computer computer1 = new Computer(Manufacturer.HP, 3456, 1100, 3, 2500 );
        Computer computer2 = new Computer(Manufacturer.APPLE, 5697, 1300, 2, 2500);
        Computer computer3 = new Computer(Manufacturer.SAMSUNG, 9874, 1500, 2, 2500);
        Computer computer4 = new Computer(Manufacturer.TOSHIBA, 5698, 1600, 2, 2500);

        computers[0] = computer;
        computers[1] = computer1;
        computers[2] = computer2;
        computers[3] = computer3;
        computers[4] = computer4;

        for (Computer comp : computers) {

            float price = comp.getPrice();//1100 + 1100 * 0.1

            comp.setPrice(price * 1.1f);

            comp.view();
        }

    }
}
