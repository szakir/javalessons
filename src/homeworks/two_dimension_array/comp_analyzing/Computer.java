package homeworks.two_dimension_array.comp_analyzing;

public class Computer {

    private Manufacturer manufacturer;
    private int serialNumber;
    private float price;
    private int quantatyCPU;
    private int frequencyCPU;

    //ClassName nameOfReference = new ClassName(incoming params or absent);
    // type name = value;
    public Computer(Manufacturer manufacturer, int serialNumber,//Manufacturer manufacturer = Manufacturer.LENOVO;
                    float price, int quantatyCPU, int frequencyCPU) {
        this.manufacturer = manufacturer;
        this.serialNumber = serialNumber;
        this.price = price;
        this.quantatyCPU = quantatyCPU;
        this.frequencyCPU = frequencyCPU;
    }

    public void setPrice(float price) {
        this.price = price;
    }


    public float getPrice() {
        return price;
    }

    public void view() {
        System.out.println(manufacturer + "\t" +
                serialNumber + "\t" +
                price + "\t" + quantatyCPU + "\t" +
                frequencyCPU);
    }

}

