package lessons.learn_enum;

public enum Country {
    AMERICA("America"), RUSSIA("Russia");

    private String shortName;

    Country(String shortName) {
        this.shortName = shortName;
    }

    public String getShortName() {
        return shortName;
    }
}


class LearnEnum {
    public void print(String str) {//America, Russia

    }

    public static void print1(Country country) {//America, Russia

    }

    public static void main(String[] args) {
        Country america = Country.AMERICA;
        Country america1 = Country.AMERICA;
        Country america2 = Country.RUSSIA;

//        System.out.println(america == america2);

//        print1(Country.RUSSIA);

//        System.out.println(america.getShortName());

        for (Country country : Country.values()) {
//            System.out.println(country.getShortName());
        }

        String str = "America";

        Country am = Country.valueOf(str.toUpperCase());

    }
}