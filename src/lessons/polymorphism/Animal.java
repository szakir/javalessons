package lessons.polymorphism;

public class Animal {
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public void sound() {
        System.out.println("Say something");
    }
}

class Pig extends Animal {

    private boolean isPyatachok;

    public Pig(boolean isPyatachok, String name) {
        super(name);
        this.isPyatachok = isPyatachok;
    }

    @Override
    public void sound() {
        System.out.println("Hryu-hryu");
    }
}

class Cow extends Animal {

    private boolean isRoga;

    public Cow(String name, boolean isRoga) {
        super(name);
        this.isRoga = isRoga;
    }

    @Override
    public void sound() {
        System.out.println("Mu-mu");
    }
}

class TestPolymorphism {
    public static void main(String[] args) {

//        int a = "";

        Animal[] animals = new Animal[3];

        Animal animal = new Animal("Some animal");

        Animal pig = new Pig(true, "Hrusha");//new Animal(), ранее связывание

        Animal cow = new Cow("Murka", true);

        animals[0] = animal;

        animals[1] = pig;

        animals[2] = cow;

        pig.sound();
    }
}
