package lessons.wrapper;

import java.util.ArrayList;

public class LearnWrapper {
    public static void main(String[] args) {
        int a = 10;//primitive type

        Integer i = 15;//reference type, int i = 15

        Integer i1 = new Integer(15);

        a = i;//int = Integer, unboxing

        i = a;//Integer = int, new Integer(a) - autoboxing

        ArrayList<Integer> integers = new ArrayList<>();

        integers.add(a);

        int i2 = Integer.parseInt("15");

        Integer b1 = 15;

        Integer b2 = new Integer(15);

        Integer b3 = Integer.valueOf(15);

        System.out.println(b1 == b3);

    }
}
