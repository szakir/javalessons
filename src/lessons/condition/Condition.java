package lessons.condition;

public class Condition {
    public static void main(String[] args) {
        int a = 10;

        int b = 7;

       /* if (a > 10) {
//            System.out.println("a > 10");
        } else if (a > 5) {
//            System.out.println("a > 5");
        } else {
//            System.out.println("a == 5 OR a < 5");
        }*/

//        if (a < 10 && b > 5) {// 0 & 1 -> 0
        if (a < 10 || b > 5) {// 1 || 1 -> 1
//            System.out.println("a < 10 && b > 5");
        }

//        ternary operator -> condition ? (statement if condition is true) :(statement if condition is false)

        int t;

//        System.out.println(t);

        t = a < 20 ? (a > 5 ? (a < 7 ? 98 : 100) : 78) : 0;

        if (a < 20) {
            if (a < 5) {
                t = 12;
            } else {
                t = 78;
            }
        } else {
            t = 0;
        }

       /* new homeworks.two_dimension_array.man_position.Man(null, "");
        new lessons.condition.Man();*/

//        System.out.println(t);


    }
}





