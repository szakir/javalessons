package lessons.learn_string;

public class LearnString {
    public static void main(String[] args) {
        String str = "Hello";//object-literal in string pool

        String str3 = "Hello";


        String str1 = new String("Hello");//object

        String str4 = new String("Hello");//object

//        System.out.println(str4 == str1);

//        System.out.println(str1.intern() == str3);

//        System.out.println(str1 == str3);

//        System.out.println(str1.equals(str3));

//        System.out.println("1" + 2 + 3);

        String result = "1" + 2 + 3;

        System.out.println(1 + 2 + "3");
    }
}
