package lessons.increment_decrement;

public class IncDec {
    public static void main(String[] args) {
        int count = 2;

//        count = count + 1;

//        count += 1;

//        System.out.println(count++);//post increment
//        System.out.println(count);//post increment

//        System.out.println(++count);//pre increment

        int r = count++ + ++count - --count;//2 + 4 - 3

        System.out.println(r);
    }
}
