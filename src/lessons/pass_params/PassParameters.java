package lessons.pass_params;

public class PassParameters {


    public static void changeInt(int value) {
        value = 15;
    }

    public static void changeObject(Foo foo) {
        foo.name = "Sabina";
    }

    public static void main(String[] args) {
        int a = 40;

        changeInt(a);//

//        System.out.println(a);

        Foo foo = new Foo();

        foo.name = "Andrey";

        changeObject(foo);//foo1

        System.out.println(foo.name);
    }
}

class Foo {
    String name;
}
