package lessons.learn_class;

public class FirstClass {
    private/*access modificator*/ int age;//field
    private long numberCard;//field

/*  access modificator return type(or void) nameOfMethod(incoming parameters or absent them) {

 }*/

    public FirstClass(int age, long numberCard) {//constructor
        this.age = age;
        this.numberCard = numberCard;//numberCard = 15
    }

    public void setAge(int newAge) {//(this, int newAge)
        int count = 5;

        if (newAge < 0) {
            System.out.println("Negative value for age");
        } else {
            this.age = newAge;
        }

    }

    public int getAge() {
        return age;
    }

    public long getNumberCard() {
        return numberCard;
    }

    public void setNumberCard(long numberCard) {
        this.numberCard = numberCard;
    }

    public void printFields() {
        System.out.println(age + " " + numberCard);
    }

}

class TestClass {
    public static void main(String[] args) {
//        type name = value;
//        type name = new type(can be parameters);

        FirstClass aClass/*reference*/ = new FirstClass(10, 15)/*object*/;

        FirstClass aClass1/*reference*/ = new FirstClass(12, 19)/*object*/;

        FirstClass firstClass = null;

        firstClass = new FirstClass(1, 5);

//        aClass.age = -45687999;
//        aClass = new FirstClass(8, 15);

        aClass.setAge(8);//setAge(this, 8)

     /*   int ageFromMethod = aClass.getAge();

        long numberCard = aClass.getNumberCard();

        System.out.println(ageFromMethod + " " + numberCard);*/

     aClass.printFields();

//        aClass.numberCard = -17;

//        aClass1.age = 12;
//        aClass1.numberCard = 18;

       /* int ageFromMethod1 = aClass.getAge();

        long numberCard2 = aClass.getNumberCard();

        System.out.println(ageFromMethod1 + " " + numberCard2);*/

//    aClass.printFields();

    }
}
