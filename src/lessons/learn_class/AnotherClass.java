package lessons.learn_class;

public class AnotherClass {
    private int age;//0

    private static String firstName;//null

    public static final String FIO = "World";

    public final String FIO_OBJCT;

    public AnotherClass anotherClass;

    {
//        FIO_OBJCT = "For Object";
//        FIO = "Hello";
        System.out.println("Non-static block initialization");
    }

    static {
//        FIO = "Hello";
//        System.out.println("Static block initialization");
        System.out.println("Hello world");
    }

    public AnotherClass(int age) {
//        FIO = "";
        FIO_OBJCT = "";
        System.out.println("Constructor");
        this.age = age;
    }

    public static String getFirstName() {
        return firstName;
    }

    public static void setFirstName(String firstName) {
        AnotherClass.firstName = firstName;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) {

    }
}

class TestStaticClass {
    public static void main(String[] args) {
        AnotherClass name = new AnotherClass(2);

        AnotherClass.setFirstName("John");

        AnotherClass name2 = new AnotherClass(4);

        System.out.println(AnotherClass.getFirstName());
    }
}
